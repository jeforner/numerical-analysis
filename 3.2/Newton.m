% The following is an implementation of Newton's Method in one variable.

function r = newton(f, g, x0, M, delta, epsilon)
    v = f(x0);
    n = 0;
    flag = true;
    
    if abs(v) < epsilon
        r = x0;
        flag = false; % In this case we do not run the following while loop since we
                      % are already close enough to a root of f 
    end
    
    disp(' n         x_n                 f(x_n)');
    while flag == true && n <= M
        n = n + 1;
        x1 = x0 - v/g(x0); % g is the derivative of f
        v = f(x1); % function evaluation at next iterate
            
        fprintf('%2d \t %12.15f \t %12.15f \n', n, x1, v);
        
        if abs(x1 - x0) < delta || abs(v) < epsilon % stopping critetia
            flag = false;
            r = x1; % our estimate of the root r
        else
            x0 = x1;
        end     
    end
    
    r = x1; % In case we leave the while-loop never having met the stopping criteria (and thus not have assigned a value to "r")
    
    fprintf('After %d interations, we find an approximate root of r = %.15f. \n', n, r)
end