% The following code is passed to the Newton's Method algorithm to solve
% for the cube root of 8.

fun_cuberoot = @(x) x^3 - 8; % function created from the above equation (whose root we seek)
fun_cubroot_prime = @(x) 3*x^2; % derivative of the above function
x0 = .5; % starting point (initial guess of a root)
M = 50; % number of desired steps of Newton's Method
delta = eps; % arbitrary tolerance
epsilon = eps; % arbitrary tolerance

test_cuberoot = newton(fun_cuberoot, fun_cubroot_prime, x0, M, delta, epsilon);