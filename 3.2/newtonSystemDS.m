% The following code is passed to the Newton's Method algorithm for
% nonlinear systems of equations to find a solution to the desired
% homogenous system.

F = @(x, y, z) [x*(1 - (x + 2*y + .5*z));
               y*(1 - (.5*x + y + 2*z));
               z*(1 - (2*x + .5*y + z))]; % matrix of equations
J = @(x, y, z) [1 - 2*y - z/2 - 2*x, -2*x, -x/2;
               -y/2, 1 - 2*y - 2*z - x/2,-2*y;
               -2*z, -z/2, 1 - y/2 - 2*z - 2*x];
x = 10; % initial x1 value
y = 11; % initial x2 value
z = 12; % initiral x3 value
M = 98; % number of iterations (minus 1 to account for while loop)
delta = eps; % tolerance
epsilon = eps; % tolerance

test_13 = newtonSystem(F, J, x, y, z, M, delta, epsilon);

syms x(t) y(t) z(t)

% Finding the heteroclinic orbits
ode1 = diff(x) == x*(1 - (x + 2*y + .5*z));
ode2 = diff(y) == y*(1 - (.5*x + y + 2*z));
ode3 = diff(z) == z*(1 - (2*x + .5*y + z));
odes = [ode1; ode2; ode3];
S = dsolve(odes)