% The following code finds the smallest positive starting value x0 for
% which Newton's Method diverges

fun = @(x) 2*x - (1 + x^2)*atan(x); % function whose root we seek
fun_prime = @(x) 1 - 2*x*atan(x); % derivative of the above function
x0 = 2; % starting point (initial guess of a root)
M = 19; % number of desired steps of Newton's Method (minus 1 to account for while loop)
delta = eps; % arbitrary tolerance
epsilon = eps; % arbitrary tolerance

test = newton(fun, fun_prime, x0, M, delta, epsilon);