% The following is an implementation of Newton's Method to solve a system of
% nonlinear equations.

function [x, y, z] = newtonSystem(F, J, x, y, z, M, delta, epsilon)
    v = F(x, y, z);
    n = 0;
    flag = true;
    
    if norm(F(x, y, z)) < epsilon
        flag = false; % In this case we do not run the following while loop
    end
     
    disp(' n         x1^n                    x2^n             ||f(x^n))||            ||h||');
    while flag == true && n <= M
        n = n + 1;
        w = -inv(J(x, y, z)); 
        h = w*v; % "distance" vector
        x1new = x + h(1); % new x1 value
        x2new = y + h(2); % new x2 value
        x3new = z + h(2); % new x3 value
        v = F(x1new, x2new, x3new); % function evaluation at the new estimate of the solution of the homogenous system
           
        fprintf('%2d \t %12.15f \t %12.15f \t %12.15f \t %12.15f \n', n, x, y, norm(F(x1new, x2new, x3new)), norm(h));
        
        x = x1new; % estimate of x1
        y = x2new; % estimate of x2
        z = x3new; % estimate of x3
        
        if norm(F(x, y, z)) < delta || norm(h) < epsilon % stopping criteria
            flag = false;
        end     
    end  
    fprintf('After %d interations, we find the following approximate solution to our homogenous system: (x, y, z) = (%.10f, %.10f, %.10f). \n', n, x, y, z)
end