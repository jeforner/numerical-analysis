% The following code is passed to the Newton's Method algorithm to solve
% the equation x^3 + 3x = 5x^2 + 7.

fun_4 = @(x) x^3 - 5*x^2 + 3*x - 7; % function created from the above equation (whose root we seek)
fun_4_prime = @(x) 3*x^2 - 10*x + 3; % derivative of the above function
x0 = 5; % starting point (initial guess of a root)
M = 9; % number of desired steps of Newton's Method (minus 1 to account for while loop)
delta = eps; % arbitrary tolerance
epsilon = eps; % arbitrary tolerance

test_4 = newton(fun_4, fun_4_prime, x0, M, delta, epsilon);

