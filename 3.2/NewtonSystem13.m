% The following code is passed to the Newton's Method algorithm for
% nonlinear systems of equations to find a solution to the desitred
% homogenous system.

F = @(x1, x2) [1 + x1.^2 - x2.^2 + exp(x1).*cos(x2);
               2.*x1.*x2 + exp(x1).*sin(x2)]; % matrix of equations f1 and f2
J = @(x1, x2) [2.*x1 + exp(x1).*cos(x2), -2.*x2 - exp(x1).*sin(x2);
               2.*x2 + exp(x1).*sin(x2), 2.*x1 + exp(x1).*cos(x2)]; % Jacobian matrix
x1 = -1; % initial x1 value
x2 = 4; % initial x2 value
M = 4; % number of iterations (minus 1 to account for while loop)
delta = eps; % tolerance
epsilon = eps; % tolerance

test_13 = newtonSystem(F, J, x1, x2, M, delta, epsilon);