% The following is an implementation of the Fast Fourier Transform


function C = fastfourier(m, f)
    
    N = 2^m;
    w = exp((-2*pi*1i)/N); % value of "w" as discussed in class
    Z = zeros(1, 2*N);
    C = Z;
    
    for k = 1 : N
        Z(k) = w^k; % different powers of "w"
        C(k) = f((2*pi*k)/N); % function evaluations at evenly-spaced points
    end
    
    for n = 1 : m
        for k = 1 : 2^(m - n - 1)
            for j = 1 : 2^n
                u = C(2^n*k + j);
                v = Z(j*2^(m - n - 1))*C(2^n*k + 2^(m - 1) + j);
                D(2^(n + 1)*k + j) = (u + v)/2;
                D(2^(n + 1)*k + j + 2^n) = (u - v)/2; 
            end
        end
        
        C = zeros(1, N);
        D = D(D~=0); % extracting nonzero terms
        
        for j = 1 : N
            C(j) = D(j);
        end
    end
end