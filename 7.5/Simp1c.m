% This script approximates the value of the function f(x) = (1 - x)^(1/4) 
% over the interval [0, 1] by running the adaptiveSimp function with the 
% values specified below.

f = @(x) (1 - x)^(1/4); % function of interest
a = 0; % left endpoint
b = 1; % right endpoint
n = 30; % number of iterations
epsilon = 5e-5; % tolerance

test1c = adaptiveSimp(f, a, b, n, epsilon);