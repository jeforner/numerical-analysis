% This is an implementation of Simpson's Adaptive Quadrature Algorithm. It
% approximates the value of the integral of a function f over an interval
% [a, b] using iteration and tolerance parameters specified by the user.

function sigma = adaptiveSimp(f, a, b, n, epsilon)

    delta = b - a; % interval width
    sigma = 0;
    h = delta / 2;
    c = (a + b)/2; % midpoint
    k = 1; % iteration 1
    ahat = f(a); % function evaluation at a
    bhat = f(b); % function evaluation at b
    chat = f(c); % function evaluation at c
    S = (h/3)*(ahat + 4*chat + bhat); % Simpson's Rule
    V = zeros(6, n);
    V(:, 1) = [a, h, ahat, chat, bhat, S];
    
    while 1 <= k && k <= n
        h = V(2, k)/2; % updating step size
        yhat = f(V(1, k) + h); % new function evaluation
        S1 = (h/3)*(V(3, k) + 4*yhat + V(4, k)); % Simpson's Rule on subinterval
        zhat = f(V(1, k) + 3*h); % new function evaluation
        S2 = (h/3)*(V(4, k) + 4*zhat + V(5, k)); % Simpson's Rule on subinterval
        
        if abs(S1 + S2 - V(6, k)) < (30*epsilon*h)/delta % i.e. if approximation is "good enough"
            sigma = sigma + S1 + S2 + (S1 + S2 - V(6, k))/15; % adding to approximation of the integral
            k = k - 1;         
            
            if k <=0
                disp(sigma)
                break  
            end
            
        else
            if k >= n % Here we have exceeded the number of prescribed iterations without a sufficiently good approximation
                disp('failure')
                break
            end
            
            % Updated values                
            vhat = V(5, k);
            V(:, k) = [V(1, k), h, V(3, k), yhat, V(4, k), S1];
            k = k + 1;
            V(:, k) = [V(1, k - 1) + 2*h, h, V(5, k - 1), zhat, vhat, S2];
        end
    end     
end