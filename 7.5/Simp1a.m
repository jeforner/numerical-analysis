% This script approximates the value of the function f(x) = x^(1/2) over
% the interval [0, 1] by running the adaptiveSimp function with the values
% specified below.

f = @(x) x^(1/2); % function of interest
a = 0; % left endpoint 
b = 1; % right endpoint
n = 15; % number of iterations
epsilon = eps; % tolerance

test1a = adaptiveSimp(f, a, b, n, epsilon);