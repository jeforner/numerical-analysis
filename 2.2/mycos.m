% The following code supplies accurate values of 1 - cos(x) for x in the
% interval [-pi, pi].

function y = mycos(x)    
   if abs(x) >= pi/3 % Calculated from the Loss of Precision Theorem
       y = 1 - cos(x);       
   else
       y = x^2/2 - x^4/24; % Truncated Taylor series used for x near zero
   end
end