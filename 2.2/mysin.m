% The following code accepts a machine number x and returns the value of
% y = x - sin(x) with nearly full machine precision.

function y = mysin(x)
    if abs(x) >= 1.9 % From the textbook (via Loss of Precision Theorem)
        y = x - sin(x);
    else
        y = x^3/6 - x^5/120; % Truncated Taylor series used for x near zero
    end
end