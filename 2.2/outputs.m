format long

x = -pi : pi/6 : pi;
y = zeros(1, length(x));
error1 = zeros(1, length(x));
z = zeros(1, length(x));
error2 = zeros(1, length(x));

for i = 1 : length(x)
    y(i) = mysin(x(i));
    error1(i) = abs([(x(i) - sin(x(i))) - y(i)]/(x(i) - sin(x(i))));
    z(i) = mycos(x(i));
    error2(i) = abs([(1 - cos(x(i))) - z(i)]/(1 - cos(x(i))));
end

T = table( x.', y.', error1.', z.', error2.');
T.Properties.VariableNames = {'Input_Values' 'mysin' 'Relative_Error_1' 'mycos' 'Relative_Error_2'}

