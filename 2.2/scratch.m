n = 1;
while((1.9)^(2*n + 3))/factorial((2*n + 3)) > 10e-9
    n = n + 1; 
end
    
disp(n)

x = .1;
while 1 - sin(x)/x < .5
    x = x + .1;
end

disp(x)