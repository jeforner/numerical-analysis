# This is a script to compute the value of the machine precision on my computer,
# in both single and double precision.

import numpy as np

for i in range(1, -100, -1):
    if 1 + 2 ** i == 1:
        print(i + 1)
        break

# x = 1 + 2**(-53)
# print(x == 1)
