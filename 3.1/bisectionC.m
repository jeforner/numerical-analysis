% The following code tests the bisection algorithm on the function y = 2^-x
% + e^x + 2cos(x) - 6 to find a root on the interval [1, 3]. 

fun_c = @(x) 2^-x + exp(x) + 2*cos(x) - 6;
a = 1; % left endpoint
b = 3; % right endpoint
delta = 5e-5; % arbitrary tolerance
epsilon = 5e-5; % arbitrary tolerance

test_c = bisection(fun_c, a, b, delta, epsilon);