% The following code tests the bisection algorithm on the function y = x^-1
% - tan(x) to find a root in the interval [0, pi/2].

fun_a = @(x) x^-1 - tan(x);
a = 0; % left endpoint
b = pi/2; % right endpoint
delta = 5e-5; % arbitrary tolerance
epsilon = 5e-5; % arbitrary tolerance

test_a = bisection(fun_a, a, b, delta, epsilon);