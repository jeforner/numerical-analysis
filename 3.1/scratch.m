c = [1/2, 1/4, zeros(1, 13)];

for n = 3 : 50
    c(n) = (c(n - 1) + c(n - 2))/2;
end

fun_test = @(x) x - 1/3;
a = 0; % left endpoint
b = 1; % right endpoint
delta = 5e-10; % arbitrary tolerance
epsilon = 5e-10; % arbitrary tolerance

%test_c = bisection(fun_test, a, b, delta, epsilon);

for n = 1 : 25
    if (2^(n + 1)) >  10^6
        break
    end
end

disp(n)