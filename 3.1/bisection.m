% The following is an implementation of the bisection algorithm. It will be
% used to approximate roots of various functions on different intervals.

function c = bisection(f, a, b, delta, epsilon)
        
u = f(a); % function evaluation at initial left endpoint "a"
v = f(b); % function evaluation at initial right endpoint "b"
e = b - a; % initial interval width
w = epsilon + 1;
n = 0;
flag = true;

    if sign(u) == sign(v)
        flag = false; % In this case, we do not run the following while loop
        fprintf('No root is guaranteed to be in the interval [a, b]')
    end

    disp(' n         c_n            f(c_n)');
    while flag == true && abs(e) >= delta && abs(w) >= epsilon
        n = n + 1;
        e = e/2; % half the current interval width
        c = a + e; % midpoint of nth interval of bisection
        w = f(c); % function evaluation at the midpoint
        
        fprintf('%2d \t %12f \t %12.8f \n', n, c, w);
        
        if sign(u) == sign(w)
            a = c; % We update "a" if the sign of f(a) and f(c) match
        end
    end
    
    if flag == true
        fprintf('After %d interations, we find that one root to this function is approximately c = %.11f\n',n, c)
    end
end