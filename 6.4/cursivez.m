% This code specifies 9 different points in the xy plane that are sampled
% from the lowercase script letter "z". A large number of values Sx(t) and 
% Sy(t) are then computed, where Sx and Sy are parametrically-defined spline
% functions. The result is then plotted.

n = 8;
t = 1 : 9; % parameter
x = [0, 1, 2, 2, 2, 3, 2, 2.5, 4]; % output x-values
y = [0, .75, 1.5, .25, -.5, -1.25, -2, -1, -.5]; % output y-values


[h, z] = splinez(n, t, x); % finding z's for Sx
S = splineeval(n, t, h, x, z); % evaluating Sx at different values of t
outputx = S(S~=0); % extracting the desired x-values Sx(t)

[h, z] = splinez(n, t, y); % finding z's for Sy
S = splineeval(n, t, h, y, z); % evaluating Sy at different values of t
outputy = S(S~=0); % extracting the desired y-values Sy(t)

plot(outputx, outputy) % plot of a cursive "z"
title('Script "z"')