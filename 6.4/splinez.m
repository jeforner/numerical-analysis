% This algorithm computes the z-values for the natural cubic spline interpolator
% for a set of values (t, y), where in this context "t" is a parameter. It
% outputs the z-values as well as a vector "h" of h-values (differences
% between consecutive t's).

function [h, z] = splinez(n, t, y)

for i = 1 : n
    h(i) = t(i + 1) - t(i); % vector of differences between consecutive t values
    b(i) = 6*(y(i + 1) - y(i))/h(i);
end


u(2) = 2*(h(1) + h(2));
v(2) = b(2) - b(1);

% Here we fill in the rest of the u and v vectors

for i = 3 : n
    u(i) = 2*(h(i) + h(i - 1)) - ((h(i - 1)^2)/u(i - 1));
    v(i) = b(i) - b(i - 1) - (h(i - 1)*v(i - 1))/u(i - 1);
end

z(n + 1) = 0; % by condition

for i = n : -1 : 2
    z(i) = (v(i) - h(i)*z(i + 1))/u(i); % computing desired z values
end

z(1) = 0; % again by condition
end