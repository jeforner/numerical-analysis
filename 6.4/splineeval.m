% This algorithm computes the value of a spline given both test data (t, y)
% and the vectors of h and z values from the "splinez" function.

function S = splineeval(n, t, h, y, z)
        
    for i = 1 : n
        for x = i : .1 : i + .9 % values at which the spline S is evaluated
         A(i) = (1/(6*h(i)))*(z(i + 1) - z(i));
         B(i) = z(i)/2;
         C(i) = -(h(i)/6)*z(i + 1) - (h(i)/3)*z(i) + (1/h(i))*(y(i + 1) - y(i));
         S(i, 10*x) = y(i) + (x - t(i)).*(C(i) + (x - t(i)).*(B(i) + (x - t(i)).*A(i))); % function evaluations
        end
    end
end