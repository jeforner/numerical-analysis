% This script uses a 4th order Taylor series to "integrate back to" the
% initial condition given in problem 5, i.e. we compare the result here to
% the original initial condition x(0) = 0.

M = 156;
h = -.01;
t = 1.56;
x = 89.234503;

disp(' k          t            x');
fprintf('%3d \t %f \t %f \n', 0, t, x);
for k = 1 : M
    x1 = 1 + x^2;
    x2 = 2*x*x1;
    x3 = 2*x*x2 + 2*(x1)^2;
    x4 = 2*x*x3 + 6*x1*x2;
    x = x + h*(x1 + (h/2)*(x2 + (h/3)*(x3 + (h/4)*(x4))));
    t = t + h;
    fprintf('%3d \t %f \t %f \n', k, t, x);
end