% This script uses a 4th order Taylor series to solve the following initial
% value problem: x' = 1 + x^2 - t^3 with x(0) = -1. The step size is h =
% .01 on the interval [a, b] = [0, 2], and so we require M = 200 steps.

M = 200;
h = .01;
t = 0;
x = -1;

disp(' k       t            x');
fprintf('%3d \t %f \t %f \n', 0, t, x);
for k = 1 : M
    x1 = 1 + x^2 - t^3;
    x2 = 2*x*x1 - 3*t^2;
    x3 = 2*x*x2 + 2*(x1)^2 - 6*t;
    x4 = 2*x*x3 + 6*x1*x2 - 6;
    x = x + h*(x1 + (h/2)*(x2 + (h/3)*(x3 + (h/4)*(x4))));
    t = t + h;
    fprintf('%3d \t %f \t %f \n', k, t, x);
end