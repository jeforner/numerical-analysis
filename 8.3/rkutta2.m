% The following script solves a given IVP using a Fourth-Order Runge-Kutta
% Method. The parameters and functions are given below and passed to the
% "rkutta" function.

f = @(t, x) -10*x + cos(t) + 10*sin(t);
t = 0;
x = 0;
h = .01;
M = 500;
u = @(t) sin(t) ;

test = rkutta(f, t, x, h, M, u);