% The following script solves a given IVP using a Fourth-Order Runge-Kutta
% Method. The parameters and functions are given below and passed to the
% "rkutta" function.

f = @(t, x) (x - x*exp(t))/(exp(t) + 1);
t = 0;
x = 3;
h = -.01;
M = 200;
u = @(t) 12*exp(t)/((exp(t) + 1)^2);

test = rkutta(f, t, x, h, M, u);