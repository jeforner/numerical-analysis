% The following is algorithm is an implementation of the Fourth-Order
% Runge-Kutta Method. Note that u is the true solution to the given IVP.

function x = rkutta(f, t, x, h, M, u)

    e = abs(u(t) - x);
    
    disp(' k           t           x           e');
    fprintf('%3d \t %f \t %f \t %f \n', 0, t, x, e);
    
    for k = 1 : M
        F1 = h*f(t, x);
        F2 = h*f(t + .5*h, x + .5*F1);
        F3 = h*f(t + .5*h, x + .5*F2);
        F4 = h*f(t + h, x + F3);
        x = x + (F1 + 2*F2 + 2*F3 + F4)/6;
        t = t + h;
        e = abs(u(t) - x);
        fprintf('%3d \t %f \t %f \t %f \n', k, t, x, e);
        
    end
end