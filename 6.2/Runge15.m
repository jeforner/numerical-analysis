% The following script computes the value of the function f(x) = 1/(1 +
% x^2) at 15 equally spaced nodes in the interval [-5, 5]. The Newton
% polynomial p of degree at most 15 is then computed. Lastly, we compute the
% error f(x) - p(x) at 30 equally spaced points in this interval.

x = -5 : 10/15 : 5;
y = zeros(1, 16);

for i = 1 : 16
    y(i) = 1/(1 + x(i)^2); % computing desired output values
end

% Here we find the polynomial of degree at most 15 that interpolates the
% data (x(i), y(i)), where i = 1, 2, ... , 15

d = zeros(1, 6);

for i = 1 : 16
    d(i) = y(i);
end

for j = 1 : 16
    for i = 16 : -1 : j + 1
        d(i) = (d(i) - d(i - 1))/(x(i) - x(i - j)); % Computing the coefficients of the Newton interpolating polynomial
    end
end

% This is our desired interpolating polynomial
p = @(x)   .0001*(x + 5)*(x + 4.333)*(x + 3.6667)*(x + 3)*(x + 2.333)*(x + 1.667)*(x + 1)*(x + .333)*(x - .333)*(x - 1)*(x - 1.6667) ...
         - .0002*(x + 5)*(x + 4.333)*(x + 3.6667)*(x + 3)*(x + 2.333)*(x + 1.667)*(x + 1)*(x + .333)*(x - .333)*(x - 1) ...
         - .0003*(x + 5)*(x + 4.333)*(x + 3.6667)*(x + 3)*(x + 2.333)*(x + 1.667)*(x + 1)*(x + .333)*(x - .333) ...
         - .0002*(x + 5)*(x + 4.333)*(x + 3.6667)*(x + 3)*(x + 2.333)*(x + 1.667)*(x + 1)*(x + .333) ...
         - .0004*(x + 5)*(x + 4.333)*(x + 3.6667)*(x + 3)*(x + 2.333)*(x + 1.667)*(x + 1) ...
         + .0002*(x + 5)*(x + 4.333)*(x + 3.6667)*(x + 3)*(x + 2.333)*(x + 1.667) ...
         + .0007*(x + 5)*(x + 4.333)*(x + 3.6667)*(x + 3)*(x + 2.333) ...
         + .0014*(x + 5)*(x + 4.333)*(x + 3.6667)*(x + 3) ...
         + .0031*(x + 5)*(x + 4.333)*(x + 3.6667) ...
         + .0074*(x + 5)*(x + 4.333) ...
         + .0182*(x + 5)...
         + .0385;

% We now compute the error between the function f and our interpolating
% polynomial p

xnew = -5: 10/(30-1) : 5;
error = zeros(1, 30);

disp(' i         error');
for i = 1 : 30
   error(i) = 1/(1 + xnew(i)^2) - p(xnew(i));
   fprintf('%2d \t %12f \n', i, error(i));
end