% The following script computes the value of the function f(x) = 1/(1 +
% x^2) at 10 equally spaced nodes in the interval [-5, 5]. The Newton
% polynomial p of degree at most 10 is then computed. Lastly, we compute the
% error f(x) - p(x) at 30 equally spaced points in this interval.

x = -5 : 10/10 : 5;
y = zeros(1, 11);

for i = 1 : 11
    y(i) = 1/(1 + x(i)^2); % computing desired output values
end

% Here we find the polynomial of degree at most 10 that interpolates the
% data (x(i), y(i)), where i = 1, 2, ... , 10

d = zeros(1, 6);

for i = 1 : 11
    d(i) = y(i);
end

for j = 1 : 11
    for i = 11 : -1 : j + 1
        d(i) = (d(i) - d(i - 1))/(x(i) - x(i - j)); % Computing the coefficients of the Newton interpolating polynomial
    end
end

% This is our desired interpolating polynomial
p = @(x) .0001*(x + 5)*(x + 4)*(x + 3)*(x + 2)*(x + 1)*x*(x - 1)*(x - 2)*(x - 3) ...
       - .0004*(x + 5)*(x + 4)*(x + 3)*(x + 2)*(x + 1)*x*(x - 1)*(x - 2) ...
       + .0011*(x + 5)*(x + 4)*(x + 3)*(x + 2)*(x + 1)*x*(x - 1) ...
       - .0011*(x + 5)*(x + 4)*(x + 3)*(x + 2)*(x + 1)*x ...
       - .0020*(x + 5)*(x + 4)*(x + 3)*(x + 2)*(x + 1) ...
       + .0043*(x + 5)*(x + 4)*(x + 3)*(x + 2) ...
       + .0063*(x + 5)*(x + 4)*(x + 3) ...
       + .0104*(x + 5)*(x + 4)...
       + .0204*(x + 5) ...
       + .0385;

% We now compute the error between the function f and our interpolating
% polynomial p

xnew = -5: 10/(30-1) : 5;
error = zeros(1, 30);

disp(' i         error');
for i = 1 : 30
   error(i) = 1/(1 + xnew(i)^2) - p(xnew(i));
   fprintf('%2d \t %12f \n', i, error(i));
end