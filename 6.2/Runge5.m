% The following script computes the value of the function f(x) = 1/(1 +
% x^2) at equally spaced nodes in the interval [-5, 5]. The Newton
% polynomial p of degree at most 5 is then computed. Lastly, we compute the
% error f(x) - p(x) at 30 equally spaced points in this interval.

x = -5 : 10/5 : 5;
y = zeros(1, 5 + 1);

for i = 1 : 6
    y(i) = 1/(1 + x(i)^2); % computing desired output values
end

% Here we find the polynomial of degree at most 5 that interpolates the
% data (x(i), y(i)), where i = 1, 2, 3, 4, and 5

d = zeros(1, 6);

for i = 1 : 6
    d(i) = y(i);
end

for j = 1 : 6
    for i = 6 : -1 : j + 1
        d(i) = (d(i) - d(i - 1))/(x(i) - x(i - j)); % Computing the coefficients of the Newton interpolating polynomial
    end
end

% This is our desired interpolating polynomial
p = @(x) 0.038461538461538 + 0.030769230769231*(x + 5) + 0.042307692307692*(x + 5)*(x + 3) ...
       - 0.015384615384615*(x + 5)*(x + 3)* (x + 1) + 0.001923076923077*(x + 5)*(x + 3)* (x + 1)*(x - 1);    

% We now compute the error between the function f and our interpolating
% polynomial p

xnew = -5: 10/(30-1) : 5;
error = zeros(1, 30);

disp(' i         error');
for i = 1 : 30
   error(i) = 1/(1 + xnew(i)^2) - p(xnew(i));
   fprintf('%2d \t %12f \n', i, error(i));
end

