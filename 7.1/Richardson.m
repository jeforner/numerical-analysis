% The function that follows uses Richarson extrapolation repeatedly 
% to approximate the value of the derivative of a function f at 
% a x-value "s".

function [k, d, r] = Richardson(f, s, h, M)
    
    d = zeros(1, M + 1);
    r = zeros(1, M + 1);
    
    for k = 1 : M + 1
        d(k) = (f(s + h) - f(s - h))/(2*h); % derivative approximation at step k
        h = h/2; % halving the value of h (i.e letting h go to 0)
    end
    
    disp(' k        d_k              r_k');
    fprintf('%2d \t %12.8f \t %12.8f \n', 1, d(1), '');
            
    for k = 2 : M + 1
        r(k) = d(k) + (d(k) - d(k - 1))/3; % new value obtained from values of the "d" vector
        fprintf('%2d \t %12.8f \t %12.8f \n', k, d(k), r(k));
    end
    
    fprintf('The derivative of f at s = %f is approximately %f. \n', s, r(10))
end