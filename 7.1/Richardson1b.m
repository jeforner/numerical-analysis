% The following code establishes the function and parameters that are
% passed to the algorithm developed from the text in which Richardson
% extrapolation is repeatedly used to calculate f'(x). In this case we are
% approximating the value of f'(arcsin(.8)), where f(x) = tan(x).

f = @(x) tan(x); % function of interest
s = asin(0.8); % x-value at which we seek the approximate value of the derivative
h = 1; % arbitrary starting h-value
M = 9; % number of interations (minus 1)

test_1b = Richardson(f, s, h, M);